<?php
header('Access-Control-Allow-Origin: *');

if(!empty($_POST['urla'])){
    $data = array();
    
    //database details
    $dbHost     = 'localhost';
    $dbUsername = 'root';
    $dbPassword = 'hunter';
    $dbName     = 'extension';
    
    //create connection and select DB
    $db = new mysqli($dbHost, $dbUsername, $dbPassword, $dbName);

    if($db->connect_error){
        die("Unable to connect database: " . $db->connect_error);
    }

    $url = $db->real_escape_string($_POST['urla']);
    $selectedText = $db->real_escape_string($_POST['selectedText']);
    $currentConceptText = $db->real_escape_string($_POST['currentConceptText']);
    $add = $db->real_escape_string($_POST['add']);
    
    //get user data from the database
    $query;

    if($currentConceptText == "") {
        $query = $db->query("INSERT INTO concepts (concept_url,concept_name,added) VALUES ('$url','$selectedText',0)");
        $data['insert'] = 'ok';
        $data['name'] = $selectedText;
    }else{
        if($add){
            $query = $db->query("UPDATE concepts set added = 0 WHERE concept_name = '$currentConceptText' and concept_url = '$url'");
            $data['add'] = TRUE;
        }else{
        $query = $db->query("UPDATE concepts set concept_name = '$selectedText' WHERE concept_name = '$currentConceptText' and concept_url = '$url'");
        }
        $data['update'] = 'ok';
        $data['name'] = $selectedText;
    }

    if($query != TRUE) {
        $data['status'] = 'error';
    }

    $query = $db->query("SELECT * FROM concepts WHERE concept_url = '$url'");
    
    if ($query->num_rows > 0) {

        if($data['status'] != 'error') {
            $data['status'] = 'ok';
        }
        
        $data['count'] = $query->num_rows;
    }else {
        $data['status'] = 'err';
        $data['result'] = '';
    }

    echo json_encode($data);
    $db->close();
}
?>