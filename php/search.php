<?php 
header("Content-type: text/html; charset=UTF-8");

$dbHost     = 'localhost';
$dbUsername = 'root';
$dbPassword = 'hunter';
$dbName     = 'extension';

$db = new mysqli($dbHost, $dbUsername, $dbPassword, $dbName);

if($db->connect_error){
    die("Unable to connect database: " . $db->connect_error);
}

$db->query("SET NAMES 'utf8'"); 
$db->query("SET CHARACTER SET 'utf8'");
$db->query("SET SESSION collation_connection = 'utf8_general_ci'");

$search =  $db->real_escape_string($_POST['search']);
$search = addslashes($search);
$search = htmlspecialchars($search);
$search = stripslashes($search);

if($search == ''){
  exit("Почніть вводити запит");
}
                
$query = $db->query("SELECT DISTINCT concept_name FROM concepts WHERE concept_name LIKE '%". $search ."%'");

if($query->num_rows > 0){
   $sql = $query->fetch_array();
   do{
     echo "<div style='font-size: 24px;'>".$sql['concept_name']."</div>"."<div style='width: 100%;height: 2px;background-color: grey;'</div>";
          
   }while($sql = $query->fetch_array());
}else{
   echo "Нема результатів";
}
$db->close();
?>