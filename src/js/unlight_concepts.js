chrome.runtime.onMessage.addListener(function(message, sender, sendResponse) {

	var tag_array = [];

	for(let i = 0;i < message.length;i++){
		let tag;
		if (message[i].includes("[")){
	    	tag = message[i].replace(/\[/g,"open_square");
	    	tag_array.push(tag)
	    }else if (message[i].includes("\\")){
	    	tag = message[i].replace(/\\/g,"slash");
	    	tag_array.push(tag);
	    }else if (message[i].includes("^")){
	    	tag = message[i].replace(/\^/g,"up");
	    	tag_array.push(tag);
	    }else if (message[i].includes("$")){
	    	tag = message[i].replace(/\$/g,"dollar");
	    	tag_array.push(tag);
	    }else if (message[i].includes(".")){
	    	tag = message[i].replace(/\./g,"point");
	    	tag_array.push(tag);
	    }else if (message[i].includes("|")){
	    	tag = message[i].replace(/\|/g,"wall");
	    	tag_array.push(tag);
	    }else if (message[i].includes("?")){
	    	tag = message[i].replace(/\?/g,"question");
	    	tag_array.push(tag);
	    }else if (message[i].includes("*")){
	    	tag = message[i].replace(/\*/g,"multiply");
	    	tag_array.push(tag);
	    }else if (message[i].includes("+")){
	    	tag = message[i].replace(/\+/g,"plus");
	    	tag_array.push(tag);
	    }else if (message[i].includes("(")){
	    	tag = message[i].replace(/\(/g,"open_bracket");
	    	tag_array.push(tag);
	    }else if (message[i].includes(")")){
	    	tag = message[i].replace(/\)/g,"close_bracket");
	    	tag_array.push(tag);
	    }else if (message[i].includes("#")){
	    	tag = message[i].replace(/#/g,"hash_tag");
	    	tag_array.push(tag);
	    }
	    else{
	    	tag = message[i];
	    	tag_array.push(message[i]);
	    }
	}

	for(let j = 0; j < message.length; j++){

		let spans = $("span[name="+ tag_array[j] +"]");

	    for(let k = 0;k < spans.length;k++){
	    	spans[k].setAttribute("style","");
	    }

	//     $('body :not(script)').contents().filter(function() {
	// 	    return this.nodeType === 1;
	// 	  }).replaceWith(function() {
	// 	  		console.log(this.nodeValue);
	// 	  		return this.nodeValue.replace(new RegExp("<span class='selected' name="+ message[j] +" style='background:#ADFF2F;font-size:24px'>"+ message[j] +"</span>",'gi'),message[j]);
	// 		});
	}

    chrome.runtime.onMessage.removeListener(arguments.callee);
  
});