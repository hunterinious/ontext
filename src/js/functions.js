class Helper{

  arr_diff(a1, a2) {
      var a = [], diff = [];
      for (var i = 0; i < a1.length; i++) {
          a[a1[i]] = true;
      }
      for (var i = 0; i < a2.length; i++) {
          if (a[a2[i]]) {
              delete a[a2[i]];
          } else {
              a[a2[i]] = true;
          }
      }
      for (var k in a) {
          diff.push(k);
      }
      return diff;
  };


  getUnique(a) {
    var b = [a[0]], i, j, tmp;
    for (i = 1; i < a.length; i++) {
      tmp = 1;
      for (j = 0; j < b.length; j++) {
        if (a[i] == b[j]) {
          tmp = 0;
          break;
        }
      }
      if (tmp) {
        b.push(a[i]);
      }
    }
    return b;
  }

  TrimStr(s) {
       s = s.replace( /^\s+/g, '');
    return s.replace( /\s+$/g, '');
  }

  TrimAllStr(s){
      return s.replace(/\s+/g,'');
  }

  Replace(s){
      if(s.includes("/")){
          return s.replace("/","")
      }else{
          return s;
      }
  }

  escapeRegExp(str) {
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
  }

  strSortLength(a, b){
      return b.length - a.length;
  }

}

module.exports = Helper;