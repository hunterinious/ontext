chrome.runtime.onMessage.addListener(function(message, sender, sendResponse) {
	   
	function TrimStr(s) {
	     s = s.replace( /^\s+/g, '');
	  return s.replace( /\s+$/g, '');
	}

	function TrimAllStr(s){
		return s.replace(/\s+/g,'');
	}

	function escapeRegExp(str) {
	  return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
	}

	function FindOnPage(name,tag) {//ищет текст на странице, в параметр передается ID поля для ввода

	  let spans = $("span[name="+ tag +"]");

	  if(spans.length > 0){
	  	for(let i =0; i < spans.length; i++){
	  		spans[i].setAttribute("style", "background:#ADFF2F;font-size:24px");
	  	}
	  	return;
	  }

	  let textToFind;
	  
	  if (name) {
	    textToFind = TrimStr(name);
	  } else {
	    alert("Введенная фраза не найдена");
	    return;
	  }
	  if (textToFind == "") {
	    alert("Вы ничего не ввели");
	    return;
	  }
	   
	$('body :not(script)').contents().filter(function() {
	    return this.nodeType === 3;
	}).replaceWith(function() {
	  		return this.nodeValue.replace(new RegExp(escapeRegExp(textToFind),'gi'),"<span class='selected' name="+tag+" style='background:#ADFF2F;font-size:24px'>"+textToFind+"</span>");
		});

	}

	function add(name){

		name = TrimStr(name);
		name = TrimAllStr(name);

		$("span[name="+ name +"]").on('click',function(event){
			if(($("div.modal").prev($("span[name="+ name +"]"))).length > 0){
				return;
			}
			
			$("<div class='modal'><div class='modal-content'><span class='close'>&times;</span><p class='find'></p></div></div>").insertAfter(event.target);

			$("div.modal").css({
			    display: "block",
			    position: "relative",
			    "z-index" : "10",
			    left: "0",
			    top: "0",
			    width: "550px",
			    height: "100%",
			    overflow: "auto",
			    "background-color": "rgb(0,0,0,0.8)",
			    "margin-bottom": "50px"
			});

			$(".modal-content").css({
				"background-color": "#fefefe",
				margin: "0px 10px 0px 50px",
				padding: "20px",
				border: "1px solid #888",
				width: "450px",
				height: "100%"
			});

			$("span.close").css({
			    color: "#aaa",
			    float: "right",
			    "font-size": "28px",
			    "font-weight": "bold"
			});


			$("span.close").on('click',function(event){

				$("div.modal").css({
					 display: "none"
				});

				$("div.modal").remove();
				$("div.modal-content").remove();
				$("span.close").remove();
				$("p.pr").remove();
			});
		});
	}

	let tag;

	if (message.includes("[")){
		tag = message.replace(/\[/g,"open_square");
	
	}else if (message.includes("\\")){
		tag = message.replace(/\\/g,"slash");

	}else if (message.includes("^")){
		tag = message.replace(/\^/g,"up");
		 
	}else if (message.includes("$")){
		tag = message.replace(/\$/g,"dollar");
		 
	}else if (message.includes(".")){
		tag = message.replace(/\./g,"point");
		 
	}else if (message.includes("|")){
		tag = message.replace(/\|/g,"wall");
		 
	}else if (message.includes("?")){
		tag = message.replace(/\?/g,"question");
		 
	}else if (message.includes("*")){
		tag = message.replace(/\*/g,"multiply");
		 
	}else if (message.includes("+")){
		tag = message.replace(/\+/g,"plus");
		 
	}else if (message.includes("(")){
		tag = message.replace(/\(/g,"open_bracket");
		 
	}else if (message.includes(")")){
		tag = message.replace(/\)/g,"close_bracket");
		 
	}else if (message.includes("#")){
		tag = message.replace(/#/g,"hash_tag");
		 
	}
	else{
		tag = message;
	}

	FindOnPage(message,tag);
	add(tag);

	chrome.runtime.onMessage.removeListener(arguments.callee);
});