class ToolButtons{

    createSaveDiv(div){
        let save = document.createElement('button');
        $(save).css({
            height: "18px",
        });
        save.className = 'inline save';
        let i1 = document.createElement('i');
        i1.className = 'fa fa-check';
        save.appendChild(i1);
        div.appendChild(save);
    }

    createDeleteDiv(div){
        let delet = document.createElement('button');
        $(delet).css({
            height: "18px",
        });
        delet.className = 'inline delet';
        let i2 = document.createElement('i');
        i2.className = 'fa fa-close';
        delet.appendChild(i2);
        div.appendChild(delet);
    }

    createConceptsToolButtons(i,name){
        let div = document.createElement('div');
        let tool = document.getElementById("concepts-tool");

        if(name.includes("/")){
            this.createSaveDiv(div);
            this.createDeleteDiv(div);
        }else{
            this.createDeleteDiv(div);
        }

        div.id = i + "concepts-tool";
        $(div).css({
            height: "17px",
            "margin-bottom": "4px"
        });
        tool.appendChild(div);
    }

    createThesisToolButtons(i,height){
        let div = document.createElement('div');
        let tool = document.getElementById("thesis-tool");

        this.createDeleteDiv(div,i);

        div.id = i + "thesis-tool";
        tool.appendChild(div);

        if(height != ""){
            $(div).css({
                height: "50%",
                "margin-bottom": "85px"
            });
        }else{
            $(div).css({
                height: "17px",
                "margin-bottom": "4.5px"
            });
        }
    }
}

module.exports = ToolButtons;