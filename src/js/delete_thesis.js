chrome.runtime.onMessage.addListener(function(m, sender, sendResponse) {

	let message = m[0];
	let text = m[1];
	   
	function TrimStr(s) {
	     s = s.replace( /^\s+/g, '');
	  return s.replace( /\s+$/g, '');
	}

	function TrimAllStr(s){
		return s.replace(/\s+/g,'');
	}

	function add(name,text){

		name = TrimStr(name);
		name = TrimAllStr(name);

		let array = $("p[name="+ name +"] p");
		let par = [];

		for(let k = 0; k < array.length;k++){
			if($(array[k])[0].children.length > 0){
				if($(array[k])[0].children[0].src == text){
					par.push(array[k]);
				}
			}else if(array[k].innerText == text){
				par.push(array[k]);
			}
		}
		
		let className = ($($(par)[0]).next()[0]).className;
		let br = $("."+className+"");

       	for(let i = 0;i < par.length;i++){
       		par[i].remove();	
       	}

       	for(let j =0; j < br.length; j++){
       		br[j].remove();
       	}

	}

	let tag;

	if (message.includes("[")){
		tag = message.replace(/\[/g,"open_square");
	
	}else if (message.includes("\\")){
		tag = message.replace(/\\/g,"slash");

	}else if (message.includes("^")){
		tag = message.replace(/\^/g,"up");
		 
	}else if (message.includes("$")){
		tag = message.replace(/\$/g,"dollar");
		 
	}else if (message.includes(".")){
		tag = message.replace(/\./g,"point");
		 
	}else if (message.includes("|")){
		tag = message.replace(/\|/g,"wall");
		 
	}else if (message.includes("?")){
		tag = message.replace(/\?/g,"question");
		 
	}else if (message.includes("*")){
		tag = message.replace(/\*/g,"multiply");
		 
	}else if (message.includes("+")){
		tag = message.replace(/\+/g,"plus");
		 
	}else if (message.includes("(")){
		tag = message.replace(/\(/g,"open_bracket");
		 
	}else if (message.includes(")")){
		tag = message.replace(/\)/g,"close_bracket");
		 
	}else if (message.includes("#")){
		tag = message.replace(/#/g,"hash_tag");
		 
	}
	else{
		tag = message;
	}

	add(tag,text);

	chrome.runtime.onMessage.removeListener(arguments.callee);
});