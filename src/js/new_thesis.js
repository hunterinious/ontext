chrome.runtime.onMessage.addListener(function(m, sender, sendResponse) {

	let message = m[0];
	let text = m[1];
	   
	function TrimStr(s) {
	     s = s.replace( /^\s+/g, '');
	  return s.replace( /\s+$/g, '');
	}

	function TrimAllStr(s){
		return s.replace(/\s+/g,'');
	}

	function add(name,text){

		name = TrimStr(name);
		name = TrimAllStr(name);

		let par = $("p[name="+ name +"]");

		for(let k = 0;k < par[0].length;k++){
			if($(par[0])[0].children[k].innerHTML == text){
				return;
			}
		}

		let children = ($($("p[name="+ name +"]")[0]).children());
		let classNames = [];

		for(let i = 0; i < children.length;i++){
			if(children[i].className.includes("br")){
				classNames.push(children[i].className.replace("br",""));
			}
		}

		let max = classNames[0];

		for(let i = 0; i < classNames.length;i++){
			if(Number(classNames[i]) > max){
				max = Number(classNames[i]);
			
			}
		}

		max++;

		let p = document.createElement('p');

		if(text.includes("http")){
			let img = document.createElement('img');
			img.src = text;
			$(img).css({
                width: "50%",
                height: "50%"
            })

            for(let j = 0; j < par.length;j++){
            	p.appendChild(img);
            	className = max + "br";
            	p.className = max + "p";
            	par[j].appendChild(p);
				par[j].innerHTML += "<br class="+className+"><br class="+className+">";
            }
		}else{
			for(let j = 0; j < par.length;j++){
				p.innerHTML = text;
				className = max + "br";
				p.className = max + "p";
            	par[j].appendChild(p);
				par[j].innerHTML += "<br class="+className+"><br class="+className+">";
			}
		}

	}

	let tag;

	if (message.includes("[")){
		tag = message.replace(/\[/g,"open_square");
	
	}else if (message.includes("\\")){
		tag = message.replace(/\\/g,"slash");

	}else if (message.includes("^")){
		tag = message.replace(/\^/g,"up");
		 
	}else if (message.includes("$")){
		tag = message.replace(/\$/g,"dollar");
		 
	}else if (message.includes(".")){
		tag = message.replace(/\./g,"point");
		 
	}else if (message.includes("|")){
		tag = message.replace(/\|/g,"wall");
		 
	}else if (message.includes("?")){
		tag = message.replace(/\?/g,"question");
		 
	}else if (message.includes("*")){
		tag = message.replace(/\*/g,"multiply");
		 
	}else if (message.includes("+")){
		tag = message.replace(/\+/g,"plus");
		 
	}else if (message.includes("(")){
		tag = message.replace(/\(/g,"open_bracket");
		 
	}else if (message.includes(")")){
		tag = message.replace(/\)/g,"close_bracket");
		 
	}else if (message.includes("#")){
		tag = message.replace(/#/g,"hash_tag");
		 
	}
	else{
		tag = message;
	}

	add(tag,text);

	chrome.runtime.onMessage.removeListener(arguments.callee);
});