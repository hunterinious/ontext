chrome.runtime.onMessage.addListener(function(message, sender, sendResponse) {
   
   function getUnique(a) {
	  var b = [a[0]], i, j, tmp;
	  for (i = 1; i < a.length; i++) {
	    tmp = 1;
	    for (j = 0; j < b.length; j++) {
	      if (a[i] == b[j]) {
	        tmp = 0;
	        break;
	      }
	    }
	    if (tmp) {
	      b.push(a[i]);
	    }
	  }
	  return b;
	}

    let spans = $("span.selected");
    let array = [];
    let concepts;

    if(spans){

	    for(let i=0;i < spans.length; i++){
	    	if($(spans[i]).attr("style") != ""){
    			array.push(spans[i].innerText);	
    		}
	    }

	    concepts = getUnique(array);
	}

	if(concepts[0] != undefined){
		sendResponse({response: concepts});
	}

	chrome.runtime.onMessage.removeListener(arguments.callee);
});