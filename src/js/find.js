chrome.runtime.onMessage.addListener(function(message, sender, sendResponse) {

	function TrimStr(s) {
	     s = s.replace( /^\s+/g, '');
	  return s.replace( /\s+$/g, '');
	}

	function TrimAllStr(s){
		return s.replace(/\s+/g,'');
	}

	function escapeRegExp(str) {
	  return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
	}

	function strSortLength(a, b){
		return b.length - a.length;
	}

	function help(name){

		$("span[name="+ name +"]").on('click',function(event){

			let modal = $($(event.target).next()[0]);
			
			modal.css({
			    position: "absolute",
			    left: "0",
			    opacity: 0.95,
			    "z-index" : "10",
			    width: "550px",
			    height: "90%",
			    overflow: "auto",
			    "background-color": "rgb(0,0,0,0.8)",
			    "margin-bottom": "50px"
			});

			$(".modal-content").css({
				"background-color": "#fefefe",
				margin: "0px 10px 0px 25px",
				padding: "20px",
				border: "1px solid #888",
				width: "500px",
				height: "100%",
				"font-size": "16px",
				"font-weight": "bold",
				"font-style": "normal",
				"font-family": "Tahoma, Geneva, sans-serif",
				"color": "black"
			});

			$("span.close").css({
			    color: "#aaa",
			    float: "right",
			    "font-size": "28px",
			    "font-weight": "bold",
			});

			modal.add("modal-content").add("span.close").css({
				visibility: "visible",
				display: "block"
			});

			$("span.close").on('click',function(event){
				modal.css({
					 display: "none"
				});
			});
		});
	}

	function FindOnPage(name,tag) {//ищет текст на странице, в параметр передается ID поля для ввода

		let check = $("span[name="+ tag +"]");

		if(check.attr("style","") && (check.length > 0)){
			check.attr("style","background:#ADFF2F;font-size:24px");
			return
		}

		let textToFind;

		if (name) {
			textToFind = TrimStr(name);
		} else {
			alert("Введенная фраза не найдена");
			return;
		}
		if (textToFind == "") {
			alert("Вы ничего не ввели");
			return;
		}
	   
		$('body :not(script)').contents().filter(function() {
		    return this.nodeType === 3;
		  }).replaceWith(function() {
		  		return this.nodeValue.replace(new RegExp(escapeRegExp(textToFind),'gi'),"<span class='selected' name="+tag+" style='background:#ADFF2F;font-size:24px'>"+textToFind+"</span>");
			});

	}

	function add(name,thesis){

		if(($("div[name="+ name +"]").prev($("span[name="+ name +"]"))).length > 0){
			help(name);
			return;
		}

		name = TrimStr(name);
		name = TrimAllStr(name);

		$("<div class='modal' name="+name+"><div class='modal-content'><span class='close'>&times;</span><p name="+name+" class='find'></p></div></div>").insertAfter($("span[name="+ name +"]"));
		$("div.modal").add("modal-content").add("span.close").css({
			visibility: "hidden",
			display: "none"
		});

		let par = $("p[name="+ name +"]");

		if(thesis.length > 0) {

			for(let i = 0; i < thesis.length;i++){

				let p = document.createElement('p');
				let className;

				if(thesis[i].includes("http")){
					let img = document.createElement('img');

					img.src = thesis[i];
					$(img).css({
	                    width: "50%",
	                    height: "50%"
	                })

	                for(let j = 0; j < par.length;j++){
	                	p.appendChild(img);
	                	className = i + "br";
	                	p.className = i + "p"
	                	par[j].appendChild(p);
						par[j].innerHTML += "<br class="+className+"><br class="+className+">";
	                }
				}else{
					for(let j = 0; j < par.length;j++){
						p.innerHTML = thesis[i];
						className = i + "br";
						p.className = i + "p"
						par[j].appendChild(p);
						par[j].innerHTML += "<br class="+className+"><br class="+className+">";
					}
				}
			}
		}
		help(name);	
	}

	var concepts;
	var thesis;

	concepts = message[0].map((item,i,data) => item.concept_name);
	concepts.sort(strSortLength);
	thesis = message[1];

	var tag_array = [];

	for(let i = 0;i < concepts.length;i++){
		let tag;
		if (concepts[i].includes("[")){
	    	tag = concepts[i].replace(/\[/g,"open_square");
	    	tag_array.push(tag)
	    }else if (concepts[i].includes("\\")){
	    	tag = concepts[i].replace(/\\/g,"slash");
	    	tag_array.push(tag);
	    }else if (concepts[i].includes("^")){
	    	tag = concepts[i].replace(/\^/g,"up");
	    	tag_array.push(tag);
	    }else if (concepts[i].includes("$")){
	    	tag = concepts[i].replace(/\$/g,"dollar");
	    	tag_array.push(tag);
	    }else if (concepts[i].includes(".")){
	    	tag = concepts[i].replace(/\./g,"point");
	    	tag_array.push(tag);
	    }else if (concepts[i].includes("|")){
	    	tag = concepts[i].replace(/\|/g,"wall");
	    	tag_array.push(tag);
	    }else if (concepts[i].includes("?")){
	    	tag = concepts[i].replace(/\?/g,"question");
	    	tag_array.push(tag);
	    }else if (concepts[i].includes("*")){
	    	tag = concepts[i].replace(/\*/g,"multiply");
	    	tag_array.push(tag);
	    }else if (concepts[i].includes("+")){
	    	tag = concepts[i].replace(/\+/g,"plus");
	    	tag_array.push(tag);
	    }else if (concepts[i].includes("(")){
	    	tag = concepts[i].replace(/\(/g,"open_bracket");
	    	tag_array.push(tag);
	    }else if (concepts[i].includes(")")){
	    	tag = concepts[i].replace(/\)/g,"close_bracket");
	    	tag_array.push(tag);
	    }else if (concepts[i].includes("#")){
	    	tag = concepts[i].replace(/#/g,"hash_tag");
	    	tag_array.push(tag);
	    }
	    else{
	    	tag = concepts[i];
	    	tag_array.push(tag);
	    }
		FindOnPage(concepts[i],tag);
	}

	for(let i = 0;i < tag_array.length;i++){
		let array_thesis = [];

		if(thesis){
			for(let j = 0;j < thesis.length; j++){
				if(thesis[j].concept_name === concepts[i]){
					array_thesis.push(thesis[j].thesis_name);
				}
			}
		}
		add(tag_array[i],array_thesis);
	}

	chrome.runtime.onMessage.removeListener(arguments.callee);
});

