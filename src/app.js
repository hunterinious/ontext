//підключення модулю допоміжних функцій
let Helper = require('./js/functions.js');
let helper = new Helper();
//підключення модулю допоміжних кнопок
let ToolButtons = require('./js/tool_buttons.js');
let toolButtons = new ToolButtons();

let pathToServer = 'http://localhost/~vladyslav/Ontext/php/'

$(document).ready(function(){

    //при скролі понять скроляться й допоміжні кнопки
    $('.column1').on('scroll', function () {
        $('.column2').scrollTop($(this).scrollTop());
    });

    let urla;
    let selectedConcepts = document.getElementById("select-concepts");
    let selectedThesis = document.getElementById("select-thesis");
    let selectedOtherThesis = document.getElementById("select-other-thesis");
    let currentConcept = document.getElementById("current-concept");
    let cancelConcept = document.getElementById("cancel-concept");
    let cancelThesis = document.getElementById("cancel-thesis");
    let deleteConcept = document.getElementById("delete-concept");
    let deleteThesis = document.getElementById("delete-thesis");
    let conceptsTool = document.getElementById("concepts-tool");
    let thesisTool = document.getElementById("thesis-tool");
    let selectedText;
    let currentThesis;
    let currentSelectedConceptText;
    let currentConceptText;

    var conceptsThesisArray = [];

    chrome.tabs.executeScript( null, {"code": "window.location.href"}, function(location) {
        urla = location[0];
    });

     chrome.tabs.executeScript( null, {"code": "window.getSelection().toString()"}, function(selection) {
        selectedText = selection[0];
        getConcepts(urla);
    });

    //функція видалення понять
    function deleteConceptF(name){
        // запит на видалення понять
        $.ajax({
            type:'POST',
            url:pathToServer + 'deleteConcepts.php',
            dataType: "json",
            data:{
                urla:urla,
                name:name
            },
            success:function(data){
                
                //якщо видалення пройшло успішно
                if(data.status == 'ok'){
                    let count = $("#select-concepts").children("option").length - 1;
                    let allOption = $("#select-concepts").children("option");

                    //цикл знаходження саме того поняття, яке потрібно видалити
                    for(i = 0; i < count + 1;i++){
                        if(allOption[i].text === name || allOption[i].text  === (name + "/")){
                            allOption[i].remove();
                            $("#"+i+"concepts-tool").remove();
                        }
                    }

                    //виведення кількості понять на єкран
                    document.getElementById('concepts-count').innerHTML = "Кількість понять:" + "  " + count;

                    //зміна видимості об'єктів
                    $("#edit-concept").add("#add-thesis").add("#thesis-count").add("#thesis-column").add("#current-concept")
                                      .add("#other-thesis-count").add("#select-other-thesis").css({
                        display: "none",
                        visibility: "hidden"
                    }) 

                    //прибрати підсвітку видаленого поняття на веб-сторінці                  
                    flyConcepts(name,'src/js/unlight_concept.js');
                //інакше  
                }else{
                    // flyConcepts("Запись не найдена",'js/alert.js');
                    // return false;

                }
            },
            error: function (request, status, error) {
                // alert(request.responseText);
            }
        });
    }

    //функція додавання поняття
    function addConceptF(text,currentConceptText,check){
        // запит на додавання поняття
         $.ajax({
            type:'POST',
            url: pathToServer + 'insertConcept.php',
            dataType: "json",
            data:{
                urla:urla,
                selectedText:text,
                currentConceptText:currentConceptText,
                add:check
            },
            success:function(data){
                
                //якщо запит успішний і тип запиту - вставка
                if(data.status == 'ok' && data.insert == 'ok'){
                    
                    let conceptsCount = document.getElementById('concepts-count');
                    let option = document.createElement('option');
                    option.id = data.count - 1;
                    option.className = 'concepts';
                    option.text = data.name;
                    selectedConcepts.appendChild(option);
                    //виведення на єкран кількості понять
                    document.getElementById('concepts-count').innerHTML = "Кількість понять:" + "  " + data.count;

                    //створення кнопки швидкого доступу для поняття
                    toolButtons.createConceptsToolButtons(data.count - 1,"");

                    //підсвітка нового поняття на веб-сторінці
                    flyConcepts(data.name,'src/js/new_concept.js');
                //якщо запит успішний і тип запиту - оновлення                     
                }else if(data.status == 'ok' && data.update == 'ok'){
                    let options= document.getElementById("select-concepts").childNodes

                    //цикл знаходження саме того поняття, текст якого треба змінити
                    options.forEach(function(option, i, arr) {
                      //якщо поняття постійне
                      if(option.text === currentConceptText){
                        option.text = data.name;
                        currentConcept.textContent = data.name;
                      }
                      //якщо поняття локальне
                      if(option.text === (currentConceptText + "/")){
                            ($("#"+option.id+"concepts-tool")[0]).firstChild.remove();
                            option.text = helper.Replace(option.text);
                      }
                    });

                    // flySearch(data.name,'js/new_concept.js')

                }
                else{
                    // alert("Concept already exist");
                }
            },
            error: function (request, status, error) {
                // alert("Concept already exist");
            }
        });
    }

    //функція видалення тез
    function deleteThesisF(name,text){
        //запит на видалення тези
        $.ajax({
            type:'POST',
            url:pathToServer + 'deleteThesis.php',
            dataType: "json",
            data:{
                urla:urla,
                name:name
            },
            success:function(data){
                //якщо видаленн тезі прошло успішно
                if(data.status == 'ok'){
                    let count = $("#select-thesis div.tblocks").length - 1;
                    let div;
                    let allDivs = $("div.tblocks");
                    //цикл знаходження саме тієї тези, яку потібно видалити
                    for(i = 0; i < count + 1;i++){
                        //якщо теза текстова
                        console.log(allDivs[i].textContent);
                        console.log(name);
                        if(allDivs[i].textContent === name){
                            // console.log(allDivs[i]);
                            div = $(allDivs[i]).next()[0];
                            // console.log(div);
                            div.remove()
                            allDivs[i].remove();
                            // console.log($("#"+i+"thesis-tool"));
                            $("#"+i+"thesis-tool").remove();

                            // let wrap = $(".twrap")
                            // for(j = 0;j < wrap.length;j++){
                            //     if (wrap[j].id === "wt" + (i + 1)){
                                    
                            //         wrap[j].remove();
                            //     }
                            // }
                        //якщо теза це малюнок
                        }else if($(allDivs[i].children).length > 0 && $(allDivs[i].children)[0].src === name){

                            div = $(allDivs[i]).next()[0];
                            div.remove()
                            allDivs[i].remove();
                            $("#"+i+"thesis-tool").remove();

                            // allDivs[i].remove();
                            // $("#"+i+"thesis-tool").remove();
                            
                            // let wrap = $(".twrap")
                            // for(j = 0;j < wrap.length;j++){
                            //     if (wrap[j].id === "wt" + (i + 1) || wrap[j].id === allDivs.length){
                            //         wrap[j].remove();
                            //     }
                            // }
                        }
                    }

                    //виведення на єкран кількості тез
                    document.getElementById('thesis-count').innerHTML = "Кількість тез:" + "  " + count; 

                    //якщо тез нема, встановити малий розмір блоку з тезами
                    if(count == 0){
                        $("#thesis-column").add("#select-thesis").add("#thesis-tool").css({
                            height: "20px"
                        }) 
                    }
                    
                    // приховати блок редагування тез
                    $("#edit-thesis").css({
                        display: "none",
                        visibility: "hidden"
                    }) 

                    flyThesis(text,name,'src/js/delete_thesis.js')
                   
                }else{
                    // alert("Запись не найдена");
                }
            },
            error: function (request, status, error) {
                // alert(request.responseText);
            }
        });
    }

    // функція видалення усіх понять з бази або сторінкових
    // видалити усі поняття з бази або тільки сторінкові визначається за парметром urls
    // якщо значення pathToServer + 'deleteAllFromPage.php', то видалити понятття сторінкові
    // якщо значення pathToServer + 'deleteAll.php', то видалити всі понятття
    function del(urls){
        // запит на видалення понять
        $.ajax({
            type:'POST',
            url:urls,
            dataType: "json",
            data:{
                urla:urla
            },
            success:function(data){
                
                //якщо видалення понять пройшло успішно
                if(data.status == 'ok'){
                    let array = [];
                    // вибірка усіх допоміжих кнопок
                    let tools = $("#concepts-tool").children();
                    
                    array.push($("#select-concepts option"));
                    array.push($("#select-thesis div"));
                    array.push($("#select-other-thesis div"));

                    //масив понять, в яких потрібно прибрати підсвітку
                    let toUnlight = [];
                    let concepts = array[0];

                    //формквання масиву понять,в яких потрібно прибрати підсвітку
                    for(let j = 0; j < concepts.length;j++){
                        toUnlight.push(helper.Replace(concepts[j].textContent));
                    }

                    // прибрати підсвітку у видалених понять
                    flyConcepts(toUnlight,'src/unlight_concepts.js');

                    // цикл видалення понять та відповідних їм тез
                    for(let i = 0;i < array.length;i++){
                        let elem = array[i];

                        for(let j=0;j< elem.length;j++){
                            elem[j].remove();
                        }
                    }

                    //цикл видалення допоміжних кнопок понять
                    for(let k = 0; k < concepts.length;k++){
                        tools[k].remove();
                    }

                    // виведення на єкран кільеості понять
                    document.getElementById('concepts-count').innerHTML = "Кількість понять:" + "0";                
                }
                else{
                    // alert("Concept already exist");
                }
            },
            error: function (request, status, error) {
                    // alert(request.responseText);
            }
        });
    } 

    // функція, що відповідає за обробку понять на веб-сторінці може приймати різні значення параметрів
    // виконує наступні дії:
    // динамічна підсвітка понять
    // динамічне прибрання підсвітки 
    function flyConcepts(name,file){
         chrome.tabs.executeScript( null, {"code": "window.document.body"}, function(html) {
            chrome.windows.getCurrent(function (currentWindow) {
                chrome.tabs.query({ active: true, windowId: currentWindow.id }, function (activeTabs) {
                    activeTabs.map(function (tab) {
                        chrome.tabs.executeScript({
                                file: 'src/jquery-3.2.1.js'
                        });
                        chrome.tabs.executeScript(tab.id, {file: file}, function() {
                            chrome.tabs.sendMessage(tab.id, name,function(response) {          
                            });
                        });

                    });
                });
            });
        })  
    }

    // функція, що відповідає за обробку тез на веб-сторінці може приймати різні значення параметрів
    // виконує наступні дії:
    // динамічне додавання тех
    // динамічна зміна тез
    // динамічне видалення тез
    function flyThesis(concept_name,thesis_name,file){
        let array = [];
        array.push(concept_name);
        array.push(thesis_name);
         chrome.tabs.executeScript( null, {"code": "window.document.body"}, function(html) {
            chrome.windows.getCurrent(function (currentWindow) {
                chrome.tabs.query({ active: true, windowId: currentWindow.id }, function (activeTabs) {
                    activeTabs.map(function (tab) {
                        chrome.tabs.executeScript({
                                file: 'src/jquery-3.2.1.js'
                        });
                        chrome.tabs.executeScript(tab.id, {file: file}, function() {
                            chrome.tabs.sendMessage(tab.id, array,function(response) {          
                            });
                        });

                    });
                });
            });
        })  
    }

    // функція, яка зчитує з бази усі тези понять
    function getData(s){

        let thesisCount = $("#select-thesis").children("div").length

        let otherCount = $("#select-other-thesis").children("div").length

        // цикл, що прибирає старий набір тез
        for(i = 0; i < thesisCount + 1; i ++) {
            let thesis = document.getElementById('t' + i);
            if(thesis != null) {
                // прибрати тезу
                thesis.remove();
            }

            let wrap = document.getElementById('wt' + i);
            if(wrap != null) {
                // прибрати відступ тези
                wrap.remove();
            }

            let tool = document.getElementById(i + 'thesis-tool');
            if(tool != null) {
                // прибрати допоміжну кнопку тези
                tool.remove();
            }
        }

         // цикл, що прибирає тези понять з інших сторінок
         for(i = 0; i < otherCount + 1; i ++) {
            let thesis_other = document.getElementById('to' + i);
            if(thesis_other != null) {
                thesis_other.remove();
            }
        }

        // відображення блоку поточного поняття
        $("#current-concept").css({
            display: "block",
            visibility: "visible"
        })   

        let text = s.target.textContent;
        currentSelectedConceptText = text;

        if(text.includes("/")){
            text = text.slice(0, text.length - 1);
        }

        // відображення тексту поняття
        document.getElementById("current-concept").innerHTML = text;

        let name = currentConcept.textContent;

        // запит усіх тез понять
        $.ajax({
            type:'POST',
            url:pathToServer + 'getListOfThesis.php',
            dataType: "json",
            data:{
                urla:urla,
                name:name
            },
            success:function(data){
                
                // якщо отримано усі тези
                if(data.status == 'ok'){
                    // якщо тез більше 0
                    if(data.count > 0) {

                        let height = [];
                        // цикл створення блоків тез
                        // розмітка блоків
                        for(i = 0; i < data.count; i ++) {
                            let div = document.createElement('div');
                            let wrap = document.createElement('div');

                            div.id = 't' + i;
                            wrap.id = 'wt' + (i + 1);
                            wrap.className = "twrap";
                            div.className = "tblocks";

                            $(div).css({
                                "margin-bottom": "2px"
                            });

                            $(wrap).css({
                                width: "100%",
                                height: "2px",
                                "background-color": "grey",
                                visibility: "visible",
                                display: "block"
                            }) 

                            // якщо теза - малюнок
                            if(data.array[i].thesis_name.includes("http")){
                                let img = document.createElement('img');
                                img.src = data.array[i].thesis_name;

                                $(img).css({
                                    width: "50%",
                                    height: "50%"
                                }) 

                                div.appendChild(img);
                                height.push($(img).css("height"));

                            }else{
                                height.push("");
                            }

                            selectedThesis.appendChild(div);
                            selectedThesis.appendChild(wrap);
                        }

                        // цикл створення блоків для тез та наповення їх вмістом
                        for(i = 0;i < data.count; i ++) {
                            if(!data.array[i].thesis_name.includes("http")){
                                let div = document.getElementById('t' + i);
                                div.textContent = data.array[i].thesis_name;
                            }
                            // створення допоміжних кнопок
                            toolButtons.createThesisToolButtons(i,height[i]);   
                        }

                        // зміниті розмір блоків тез
                        $("#thesis-column").add("#select-thesis").add("#thesis-tool").css({
                            height: "200px"
                        }) 
                    }
                    // виведення кількості тез на єкран
                    document.getElementById('thesis-count').innerHTML = "Кількість тез:" + "  " + data.count; 
                   
                }else{
                     // виведення кількості тез на єкран
                    document.getElementById('thesis-count').innerHTML = "Кількість тез:" + "  " + "0"
                     // зміниті розмір блоків тез
                    $("#thesis-column").add("#select-thesis").add("#thesis-tool").css({
                        height: "20px"
                    })   
                }
            },
            error: function (request, status, error) {
                // alert(request.responseText);
            }
        });

        // запит на отримання тез понять з інших сторінок
        $.ajax({
            type:'POST',
            url:pathToServer + 'getListOfOtherThesis.php',
            dataType: "json",
            data:{
                name:name,
                urla:urla
            },
            success:function(data){
                // якщо запит успішний
                if(data.status == 'ok'){
                    // якщо кількість тез більше 0
                    if(data.count > 0) {
                        // цикл створення блоків тез
                        // розмітка блоків
                        for(i = 0; i < data.count; i ++) {
                            let div = document.createElement('div');
                            let wrap = document.createElement('div');
                            div.id = 'to' + i;
                            wrap.id = 'wto' + i;
                            wrap.className = "towrap";
                            div.className = "toblocks";

                            $(div).css({
                                  "margin-bottom": "15px"
                            });

                            $(wrap).css({
                                width: "100%",
                                height: "2px",
                                "background-color": "grey",
                                visibility: "visible",
                                display: "block"
                            }) 

                            if(data.array[i].thesis_name.includes("http")){
                                let img = document.createElement('img');
                                img.src = data.array[i].thesis_name;

                                $(img).css({
                                    width: "50%",
                                    height: "50%"
                                })  

                                div.appendChild(img);
                            }

                            selectedOtherThesis.appendChild(div);
                            selectedOtherThesis.appendChild(wrap);
                        }
                         // цикл створення блоків для тез та наповення їх вмістом
                        for(i = 0;i < data.count; i ++) {
                            if(!data.array[i].thesis_name.includes("http")){
                                let div = document.getElementById('to' + i);
                                div.textContent = data.array[i].thesis_name;
                            }
                        }
                        // зміниті розмір блоків тез
                        $("#select-other-thesis").css({
                            height: "200px"
                        })  
                    }
                    // виведення кількості тез на єкран
                    document.getElementById('other-thesis-count').innerHTML = "Кількість інших тез:" + "  " + data.count; 
                   
                }else{
                    // виведення кількості тез на єкран
                    document.getElementById('other-thesis-count').innerHTML = "Кількість інших тез:" + "  " + "0"
                    // зміниті розмір блоків тез
                    $("#select-other-thesis").css({
                        height: "20px"
                    })   
                }
            },
            error: function (request, status, error) {
                 console.log(data.status);
                // alert(request.responseText);
            }
        });  
    }
    
    // фунція отримання з бази усіх понять
    function getConcepts(urla){
         // запит понять з бази
         $.ajax({
            type:'POST',
            url:pathToServer + 'getListOfConcepts.php',
            dataType: "json",
            async:false,
            data:{
                urla:urla
            },
            success:function(data){
                // якщо запит успішний
                if(data.status == 'ok'){
                    // якщо кількість понять більше 0
                    if(data.count > 0) {
                        // цикл створення блоків понять
                        for(i = 0; i < data.count ; i ++) {
                            let option = document.createElement('option');
                            option.id = i;
                            option.className = "concepts";
                            $(option).css({
                                height: "17px"
                            });
                            selectedConcepts.appendChild(option);
                        }
                        // цикл наповнення понять текстом
                        for(i = 0;i < data.count;i ++) {
                            let option = document.getElementById(i);
                            option.text = data.array[i].concept_name  

                            if(data.array[i].added == 1){
                                option.text += "/";
                            }
                            // створення допоіжних кнопок понять
                            toolButtons.createConceptsToolButtons(i,option.text);
                        }
                    }
                    // виведення на єкран кількості понять
                    document.getElementById('concepts-count').innerHTML = "Кількість понять:" + "  " + data.count;
                   
                }else{
                    // alert("Concepts not found...");
                }
            },
            error: function (request, status, error) {
                // alert(request.responseText);
            }
        });
     }

    // підсвітка понять, при натисканні кнопки 'підсвітити поняття'
    $('#light').on('click',function(){
        // запит понять з бази
        $.ajax({
            type:'POST',
            url:pathToServer + 'getAllLists.php',
            dataType: "json",
            async:false,
            data:{
                urla:urla
            },
            success:function(data){
                //якщо запит успішний
                if(data.status == 'ok'){
                    conceptsThesisArray.push(data.concepts);
                    conceptsThesisArray.push(data.thesis);
                }else{
                    
                }
            },
            error: function (request, status, error) {
                alert(request.responseText);
            }
        });

        //передача масиву віповдниз знайдени понять для підсвітки і відповідних їм тех
        flyConcepts(conceptsThesisArray,'src/js/find.js')
    });

    // видалення усіх сторінкових понять
    $('#delete-all-from-page').on('click',function(){
        del(pathToServer + 'deleteAllFromPage.php');
    });

    // видалення усіх понять з бази
    $('#delete-all').on('click',function(){
        del(pathToServer + 'deleteAll.php');
    });

    // додати до поточної сторінки поняття, які є в базі на інших сторінках і знайдені на поточній
    $('#add-new-concepts').on('click',function(){
        chrome.tabs.executeScript( null, {"code": "window.document.body"}, function(html) {
            chrome.windows.getCurrent(function (currentWindow) {
                chrome.tabs.query({ active: true, windowId: currentWindow.id }, function (activeTabs) {
                    activeTabs.map(function (tab) {
                        chrome.tabs.executeScript(tab.id, {file: 'src/js/new_concepts.js'}, function() {
                            chrome.tabs.sendMessage(tab.id, 'value',function(response) {
                                // вибірка усіх понять має довжину більше 0
                                if(response && response.response.length > 0) {

                                    let concepts = document.getElementsByClassName("concepts");
                                    let conceptsText = [];
                                    let newConcepts = [];

                                    //якщо понять поточної сторінки більше 0
                                    if(concepts.length > 0){
                                        // сстворення масиву тексту понять
                                        for(i = 0; i < concepts.length ; i++){
                                            conceptsText.push(helper.Replace(concepts[i].text));
                                        }
                                        // об'єднання понять з інших сторінкок та понять поточної сторінки
                                        newConcepts = response.response.concat(conceptsText);
                                        // отримання унікаьного масиву понять
                                        newConcepts = helper.getUnique(newConcepts);
                                        // отримання різниці між масивами понять поточної сторінки та унікального масиву понять
                                        newConcepts = helper.arr_diff(newConcepts,conceptsText);
                                    // на поточній сторінці немає понять   
                                    }else {
                                        newConcepts = response.response;
                                    }

                                    let length = newConcepts.length;
                                    // якщо є нові поняття
                                    if(length!=0){
                                        // запит на закріплення нових понять за сторінкою
                                        $.ajax({
                                            type:'POST',
                                            url:pathToServer + 'insertAddedConcepts.php',
                                            dataType: "json",
                                            data:{
                                                urla:urla,
                                                newConcepts:newConcepts
                                            },
                                            success:function(data){
                                                // якщо запит успішний і тип запиту - вставка поняття
                                                if(data.status == 'ok' && data.insert == 'ok'){
                                                    let count = 0;
                                                    //цикл створення блоків понять та наповенння їх вмістом
                                                    for(i = conceptsText.length ; i < data.count + conceptsText.length;i++){
                                                        let option = document.createElement('option');
                                                        option.id = i;
                                                        option.className = "concepts";
                                                        selectedConcepts.appendChild(option);
                                                        option.text = newConcepts[count] + "/";
                                                        // створення допоміжних кнопок понять
                                                        toolButtons.createConceptsToolButtons(option.id,option.text)
                                                        count++;
                                                    }   
                                                    // виведення кількості понять на єкран
                                                    document.getElementById('concepts-count').innerHTML = "Кількість понять:" + "  " + (data.count + conceptsText.length);                
                                                }
                                                else{
                                                    // alert("Concept already exist");
                                                }
                                            },
                                            error: function (request, status, error) {
                                                    // alert(request.responseText);
                                            }
                                        });
                                    }                           
                                 }
                            });
                        });

                    });
                });
            });
        })  
    });

    // закріпити поняття з інших сторінок за поточною
    $('#save-concepts').on('click',function(){   
        // запит на закріплення понять за поточно сторінкою
        $.ajax({
            type:'POST',
            url:pathToServer + 'toCurrentConcepts.php',
            dataType: "json",
            data:{
                urla:urla
            },
            success:function(data){
                // якщо запит успішний
                if(data.update == 'ok'){
                    let concepts = $(".concepts");
                    let buttons = $("#concepts-tool").children().children("button.save");
                    // прибрати кнопки, що відповідають за переведння поняття в стан постійного
                    for (j = 0 ;j < buttons.length; j++){
                        buttons[j].remove();
                    }
                    // змінити текст поняття з локального на постійний
                    for(i = 0; i < concepts.length; i++){
                        if(concepts[i].text.includes("/")){
                            concepts[i].text = helper.Replace(concepts[i].text);
                        }
                    }                 
                }else{
                    
                }
            },
            error: function (request, status, error) {
                // alert(request.responseText);
            }
        });       
    });

    // видалити усі локальні поняття
    $('#delete-new-concepts').on('click',function(){   
        // запит на видалення локальних понять
        $.ajax({
            type:'POST',
            url:pathToServer + 'deleteAddedConcepts.php',
            dataType: "json",
            data:{
                urla:urla
            },
            success:function(data){
                // якщо запит успішний
                if(data.status == 'ok'){
                    let concepts = $(".concepts");
                    let tools = $("#concepts-tool").children();
                    let count = 0;

                    let toUnlight = [];
                    // цикл що прибирає поняття з додатку
                    // також формує масив понять, в яких потрібно прибрати підсвітку
                    for(i = 0; i < concepts.length; i++){
                        if(concepts[i].text.includes("/")){
                            toUnlight.push(helper.Replace(concepts[i].text));
                            concepts[i].remove();
                            tools[i].remove();
                            count++;
                        }
                    }

                    // прибрати підсвітку понять, що видалені
                    flyConcepts(toUnlight,'src/js/unlight_concepts.js');
                    // виведення кількості понять на єкран
                    document.getElementById('concepts-count').innerHTML = "Кількість понять:" + "  " + (concepts.length - count);
                    // зробити блок поточного поняття невидимим
                    $("#current-concept").css({
                        display: "none",
                        visibility: "hidden"
                    })
                                      
                }else{
                    
                }
            },
            error: function (request, status, error) {
                alert(request.responseText);
            }
        });       
    });

    // обробник події кліку на відповідне поняття в зеленому блоку понять
    selectedConcepts.addEventListener("click", function select(s){
        if(s.target.tagName != "OPTION"){
            return;
        }
        // отримання тез відповідного поняття
        getData(s);

    }, false)

    //обробник кліку на поточне вибранне поняття
    currentConcept.addEventListener("click", function select(c){
        // зробити видимим блоки з тезами поняття
        $("#edit-concept").add("#add-thesis").add("#thesis-count").add("#thesis-column")
                          .add("#other-thesis-count").add("#select-other-thesis").css({
            display: "block",
            visibility: "visible"
        })

        let text = c.target.textContent;
        $("#input-concept").val(text);  

    }, false)

    // обробник події кліку на кнопку 'нове поняття'
    $('#add-concept').on('click',function(){

        $("#edit-concept").css({
            display: "block",
            visibility: "visible"
        })

        $("#input-concept").val(selectedText);  

         currentConceptText = "";
    });

     // обробник події кліку на кнопку 'зберегти'
    $('#confirm-add-concept').on('click',function(){

        let text = $("#input-concept").val();

        if(text == ""){
            return;
        }

        text = helper.TrimStr(text);
        let check = false;
        
        if(currentSelectedConceptText && currentSelectedConceptText.includes("/")){
            check = true;
        }

        if(currentConceptText != ""){
            currentConceptText = currentConcept.textContent
        }

        addConceptF(text,currentConceptText,check); 
    });

    // обробник події кліку на допоміжні кнопки понять
    conceptsTool.addEventListener("click", function(event){

        let className = event.target.className

        //клік по кнопці зберігти
        if(className == 'inline save'){
            let id = ($(event.target).parent()[0]).id.replace("concepts-tool","");
            let name = helper.Replace(($("#"+id+"")[0]).textContent);
            addConceptF("",name,true)
        //клік по зображенню на кнопці
        }else if(className == 'fa fa-check'){
            let id = ($(event.target).parent().parent()[0]).id.replace("concepts-tool","");
            let name = helper.Replace(($("#"+id+"")[0]).textContent);
            addConceptF("",name,true)
        //клік по кнопці видалити
        }else if(className == 'inline delet'){
            let id = ($(event.target).parent()[0]).id.replace("concepts-tool","");
            let name =  helper.Replace(($("#"+id+"")[0]).textContent);
            deleteConceptF(name)
        //клік по зображенню на кнопці
        }else if(className == 'fa fa-close'){
            let id = ($(event.target).parent().parent()[0]).id.replace("concepts-tool","");
            let name =  helper.Replace(($("#"+id+"")[0]).textContent);
            deleteConceptF(name)
        }
    })

    // обробник кліку 'скасувати' редагування поняття
    cancelConcept.addEventListener("click", function select(c){
        $("#edit-concept").add("#add-thesis").add("#thesis-count").add("#thesis-column").add("#current-concept").css({
            display: "none",
            visibility: "hidden"
        }) 
    }, false)

    // обробник кліку 'видалити'' поняття
    deleteConcept.addEventListener("click", function select(c){

        let name =  $("#input-concept").val();
        name = helper.TrimStr(name);
        // виклкик функціх видалення поняття
        deleteConceptF(name);
        
    }, false)

     //обробник кліку на поточну вибрану тезу
    selectedThesis.addEventListener("click", function select(s){

        let id = s.target.id;
        let text;
        if(s.target.src){
            text = s.target.src;
        }else{
            text = s.target.textContent;
        }

        if(id == "select-thesis"){
            return;
        }

        $("#edit-thesis").css({
            display: "block",
            visibility: "visible"
        })

        $("#input-thesis").val(text); 

        currentThesis = text; 
    }, false)

    // обробник кліку 'нова теза'
    $('#add-thesis').on('click',function(){

        $("#edit-thesis").css({
            display: "block",
            visibility: "visible"
        })

        $("#input-thesis").val(selectedText);  
        
        currentThesis = "";
    });

    // обробник кліку 'зберегти' тезу
    $('#confirm-add-thesis').on('click',function(){

        let name = currentConcept.textContent;
        let text = $("#input-thesis").val();
        text = helper.TrimStr(text);

        if(text == ""){
            return;
        }
        
        let thesisText = currentThesis;

        // запит на додавання тези
        $.ajax({
            type:'POST',
            url:pathToServer + 'insertThesis.php',
            dataType: "json",
            data:{
                urla:urla,
                selectedText:text,
                name:name,
                selectedThesisText:thesisText
            },
            success:function(data){      
                // якщо статус успішний і тип запиту - вставка
                if(data.status == 'ok' && data.insert == 'ok'){ 
                    let div = document.createElement('div');
                    let wrap = document.createElement('div');
                    let height;

                    div.id = "t" + (data.count - 1);
                    wrap.id = "wt" + data.count;
                    wrap.className = "twrap";
                    div.className = "tblocks";

                    if(data.name.includes("http")){
                        let img = document.createElement('img');
                        img.src = data.name;
                        $(img).css({
                            width: "50%",
                            height: "50%"
                        })  
                        div.appendChild(img);
                        height = $(img).css("height");
                    }else{
                        data.name =  data.name.replace(/\\/g,"");
                        div.textContent = data.name;
                    }

                    selectedThesis.appendChild(div);
                    selectedThesis.appendChild(wrap);

                    // виведення на єкран кількості тез
                    document.getElementById('thesis-count').innerHTML = "Кількість тез:" + "  " + data.count; 

                    $("#thesis-column").css({
                        height: "200px"
                    })  

                    $(wrap).css({
                        width: "100%",
                        height: "2px",
                        "background-color": "grey",
                        visibility: "visible",
                        display: "block"
                    })

                    //якщо тип тези- малюнок
                    if(height){
                        toolButtons.createThesisToolButtons(data.count - 1,height);  
                    //якшо тип тези - текст
                    }else{
                        toolButtons.createThesisToolButtons(data.count - 1,"");
                    }

                    // задання розміру блоку з тезами
                    $("#thesis-column").add("#select-thesis").add("#thesis-tool").css({
                            height: "200px"
                    }) 

                    // динамічне додавання тез на сторінку
                    flyThesis(name,data.name,'src/js/new_thesis.js')
                // якщо статус успішний і тип запиту - редагування
                }else if(data.status == 'ok' && data.update == 'ok'){
            
                        let divs = document.getElementById("select-thesis").childNodes

                        divs.forEach(function(div, i, arr) {
                          if(div.textContent === thesisText){
                            data.name =  data.name.replace(/\\/g,"");
                            div.textContent = data.name;
                          }
                        });

                        let dataArray = [];
                        dataArray.push(data.name);
                        dataArray.push(data.oldName);
                     // динамічне редагування тез на сторінці
                    flyThesis(name,dataArray,'src/js/edit_thesis.js')

                }else{

                }
            },
            error: function (request, status, error) {
                    // alert(request.responseText);
            }
        });
    });


    //обробник кліку по допоміжних кнопках тех
    thesisTool.addEventListener("click", function(event){

        let className = event.target.className
        let text = currentConcept.innerHTML;
        // клік по кнопці
        if(className == 'inline delet'){
            let id = ($(event.target).parent()[0]).id.replace("thesis-tool","");
            let name;

             // console.log($(event.target).parent()[0]);
             // console.log(id);
             // console.log($("#t"+id+""));
             // console.log($("#t"+id+"")[0]);
             // console.log(($("#t"+id+"")[0]).children);
             // console.log("------------------");

            if(($("#t"+id+"")[0]).children.length > 0){
                name = ($("#t"+id+"")[0]).children[0].src;
            }else{
                name = ($("#t"+id+"")[0]).textContent;
            }
            deleteThesisF(name,text)
        //клік по зображенню на кнопці
        }else if(className == 'fa fa-close'){
            let id = ($(event.target).parent().parent()[0]).id.replace("thesis-tool","");
            let name;
               // console.log($("#t"+id+"")[0]);
            if(($("#t"+id+"")[0]).children.length > 0){
                name = ($("#t"+id+"")[0]).children[0].src;
            }else{
                name = ($("#t"+id+"")[0]).textContent;
            }
            deleteThesisF(name,text)
        }

    })

    // обробник кліку 'скасувати' редагування тези
    cancelThesis.addEventListener("click", function select(c){
        $("#edit-thesis").css({
            display: "none",
            visibility: "hidden"
        }) 
    }, false)

    // обробник кліку 'видалити' тезу
    deleteThesis.addEventListener("click", function select(c){

        let name = $("#input-thesis").val();
        let text = currentConcept.innerHTML;

        deleteThesisF(name,text);
        
    }, false)

    // пошук понять на назвою
    $(function(){
      $("#search").keyup(function(){
        var search = $("#search").val();
         // запит відповідного шуканого поняття
         $.ajax({
           type: "POST",
           url: "http://localhost/~vladyslav/Ontext/php/search.php",
           cache: false,                                
           data: {"search": search},                               
           success: function(response){
              $("#resSearch").html(response);
           }
         });
         return false;
       });

        let div = document.getElementById("resSearch")

        div.addEventListener("click", function select(s){
            if(s.target.textContent == "Начните вводить запрос"){
                return;
            }
            currentConcept.textContent = s.target.textContent;  
            getData(s);
        });
    });
});